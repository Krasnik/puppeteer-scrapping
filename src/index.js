require('dotenv').config()

const puppeteer = require('puppeteer');

const scrappingUrl = process.env.SCRAPPING_URL;
(async () => {
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    await page.setViewport({ width: 1920, height: 926 });
    await page.goto(scrappingUrl);

    // get hotel details
    const flightsData = await page.evaluate(() => {
        const flightsList = document.querySelectorAll('div.eva-3-cluster-basic.-eva-3-shadow-line-hover')
        const flightsData = Object.keys(flightsList)
            .map(listKey => {
                const days = flightsList[listKey].querySelector('div.cluster-range-landing').innerText || 'NO';
                const dates = flightsList[listKey].querySelectorAll('span.route-info-item.route-info-item-date.date');
                const price = flightsList[listKey].querySelector('span.amount.price-amount').innerText || '0';
                return {
                    days,
                    start: dates[0].innerText,
                    back: dates[1].innerText,
                    price
                };
            });
        return flightsData;
    });

    console.dir(flightsData);
})();